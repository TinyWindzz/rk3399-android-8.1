/*
 * Copyright (c) 2018 FriendlyElec Computer Tech. Co., Ltd.
 * (http://www.friendlyarm.com)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/dts-v1/;
#include "rk3399-nanopi4-common.dtsi"

/ {
	model = "FriendlyElec SOM-RK3399";
	compatible = "friendlyelec,som-rk3399", "rockchip,rk3399";

	hdmiin-sound {
		compatible = "rockchip,rockchip-rt5651-tc358749x-sound";
		rockchip,cpu = <&i2s1>;
		rockchip,codec = <&rt5651 &rt5651 &tc358749x>;
		rockchip,hp-det-gpio = <&gpio4 28 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&hp_det>;
		status = "okay";
	};
};

&mach {
	hwrev = <6>;
	model = "SOM-RK3399";
};

&i2c1 {
	tc358749x: tc358749x@0f {
		compatible = "toshiba,tc358749x";
		reg = <0x0f>;
		power33-gpios = <&gpio1 0 GPIO_ACTIVE_HIGH>;
		stanby-gpios = <&gpio2 6 GPIO_ACTIVE_HIGH>;
		reset-gpios = <&gpio2 5 GPIO_ACTIVE_HIGH>;
		int-gpios = <&gpio0 2 GPIO_ACTIVE_HIGH>;
		pinctrl-names = "default";
		pinctrl-0 = <&hdmiin_gpios>;
		status = "okay";
	};
};

&i2s1 {
	status = "okay";
	rockchip,i2s-broken-burst-len;
	rockchip,playback-channels = <2>;
	rockchip,capture-channels = <2>;
	#sound-dai-cells = <0>;
};

&rt5651_card {
	status = "disabled";

	simple-audio-card,cpu {
		sound-dai = <&i2s1>;
	};
};

&pinctrl {
	hdmiin {
		hdmiin_gpios: hdmiin-gpios {
			rockchip,pins =
				<1 0 RK_FUNC_GPIO &pcfg_output_high>,
				<2 6 RK_FUNC_GPIO &pcfg_output_high>,
				<2 5 RK_FUNC_GPIO &pcfg_output_high>,
				<0 2 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};
};
